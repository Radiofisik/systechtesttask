import Vue from 'vue'
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import Vue2Filters from 'vue2-filters'

import App from './App.vue'
import EmployeeAdd from './components/employeeadd/index.vue'
import EmployeeEdit from './components/employeeedit/index.vue'
import EmployeeLIst from './components/employeelist/index.vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(VueRouter);
Vue.use(BootstrapVue);
Vue.use(Vue2Filters);

let router = new VueRouter({
  routes: [
    { name: '/', path: '/', component: EmployeeLIst },
    { name: 'add', path: '/add:id?', component: EmployeeAdd, props: true },
    { name: 'edit', path: '/edit:id', component: EmployeeEdit, props: true }
  ]
});

new Vue({
  el: '#app',
  router: router,
  render: h => h(App)
});
