import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';

Vue.use(Vuex);

let store = new Vuex.Store({
  state: {
    date: new Date(),
    employeeList: [],
  },

  mutations: {
    setEmployeeList(state, payload) {
      state.employeeList = payload;
    },
    setEmployee(state, payload) {
      const foundIndex = state.employeeList.findIndex(x => x.id === payload.id);
      state.employeeList.splice(foundIndex, 1, payload);
    },
    addEmployee(state, payload) {
      state.employeeList.add(payload);
    },
    deleteEmployee(state, payload) {
      const foundIndex = state.employeeList.findIndex(x => x.id === payload.id);
      state.employeeList.splice(foundIndex, 1);
    },
    setDate(state, payload) {
      state.date = payload;
    }
  },

  getters: {
    employeeList(state) {
      return state.employeeList;
    },
    employeeById(state) {
      return (id) => state.employeeList.find(x => x.id === id);
    },
    getDate(state) {
      return state.date;
    },
    getCommonSalary(state) {
      //it is ok for now but in the futore it should be get from server because of pagination
      return state.employeeList.reduce((acc, currentValue, index, array) => (acc + currentValue.currentSalary), 0);
    }
  },

  actions: {
    init({ dispatch, state }) {
      if (state.employeeList.length > 0) return;
      dispatch('getAll');
    },
    getAll({ commit, state }) {
      axios.get(`/api/employees/at`, { params: { date: state.date } })
        .then(response => {
          commit('setEmployeeList',
            response.data.map(x => {
              x.hireDate = new Date(x.hireDate);
              return x;
            })
          );
        })
        .catch(e => {
          console.log(e);
        });
    },
    addEmployee({ commit, dispatch }, employee) {
      return axios.post('/api/employee', employee)
        .then(response => {
//          unfortunatelly we cannot do it that way because salary depends on salary of subemployees
//          var resp = response.data;
//          resp.hireDate = new Date(resp.hireDate);
//          commit('addEmployee', resp);
          dispatch('getAll');
        })
        .catch(e => {
          console.log(e);
        });
    },
    saveEmployee({ commit, dispatch }, employee) {
      return axios.put('/api/employee', employee)
        .then(response => {
//          unfortunatelly we cannot do it that way because salary depends on salary of subemployees
//          var resp = response.data;
//          resp.hireDate = new Date(resp.hireDate);
//          commit('setEmployee', resp);
          dispatch('getAll');
        })
        .catch(e => {
          console.log(e);
        });
    },
    deleteEmployee({ commit, dispatch }, employee) {
      return axios.delete('/api/employee', { data: employee })
        .then(response => {
//          unfortunatelly we cannot do it that way because salary depends on salary of subemployees
//          commit('deleteEmployee', employee);
          dispatch('getAll');
        })
        .catch(e => {
          console.log(e);
        });
    },
    changeDate({ commit, dispatch }, date) {
      commit('setDate', date);
      dispatch('getAll');

    }
  }

});

export default store;
