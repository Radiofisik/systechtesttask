import employeeStore from '../../stores/Employee/index.js'
import Datepicker from 'vuejs-datepicker';
import Vue from 'vue'
import moment from 'moment'

export default {
  name: 'employeelist',
  components: {Datepicker},
  props: [],
  data () {
    return {
      date: new Date(),
      fields: [
        {
          key: 'hireDate',
          sortable: false,
          label: 'Hire date',
          formatter: x => moment(String(x)).format('DD.MM.YYYY') //TODO: make configurable
        },
        {
          key: 'name',
          sortable: false,
          label: 'Name'
        },
        {
          key: 'currentSalary',
          sortable: false,
          label: 'Salary',
          formatter: x => Vue.filter('currency')(x, '₽', 0) //TODO: make configurable
        },
        {
          key: 'buttons',
          sortable: false,
          label: ''
        },
      ]
    }
  },
  computed: {
    employee: () => employeeStore.getters.employeeList,
    commonSalary: ()=> employeeStore.getters.getCommonSalary
  },
  mounted () {
    employeeStore.dispatch("init");
  },
  methods: {
    onEdit(item) {
      console.log("edit", item);
      this.$router.push({ name: 'edit', params: { id: item.id } });
    },
    onDelete(item) {
      console.log("delete", item);
      employeeStore.dispatch("deleteEmployee", item);
    },
    onAddSubemployee(item) {
      console.log("add sub", item);
      this.$router.push({ name: 'add', params: { id: item.id } });
    },
    onDateChange(date) {
      employeeStore.dispatch("changeDate", date);
    }
  }
}
