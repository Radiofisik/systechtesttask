import Datepicker from 'vuejs-datepicker';
import employeeStore from '../../stores/Employee/index.js'

export default {
  name: 'employeeadd',
  components: {
    Datepicker
  },
  props: ['id'],
  data () {
    return {
      employee: {
        managerId: this.id,
        name: '',
        type: 'Employee',
        hireDate: '',
        baseSalary: 0
      },
      //TODO: move to more common place to comply with DRY
      types: [
        { text: 'Employee', value: 'Employee' },
        { text: 'Manager', value: 'Manager' },
        { text: 'Salesman', value: 'Salesman' },
      ],
    }
  },
  computed: {
  },
  mounted () {

  },
  methods: {
    onSubmit() {
      employeeStore.dispatch("addEmployee", this.employee).then(() => {
      this.$router.go(-1);
      });
    },

    onReset: () => {
    }
  }
}
