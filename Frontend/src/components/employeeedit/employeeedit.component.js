import Datepicker from 'vuejs-datepicker';
import employeeStore from '../../stores/Employee/index.js'

export default {
  name: 'employeeedit',
  components: {Datepicker}, 
  props: ['id'],
  data() {
    return {
      employee: {},
      //TODO: move to more common place to comply with DRY
      types: [
        { text: 'Employee', value: 'Employee' },
        { text: 'Manager', value: 'Manager' },
        { text: 'Salesman', value: 'Salesman' },
      ],
    }
  },
  computed: {
  },
  mounted() {
    this.employee = Object.assign({}, employeeStore.getters.employeeById(this.id));
  },
  methods: {
    onSubmit() {
      employeeStore.dispatch("saveEmployee", this.employee);
      this.$router.go(-1);
    },
    onReset() {
      this.employee = Object.assign({}, employeeStore.getters.employeeById(this.id));
    }
  }
}
