﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.DAL;
using Core.Init;
using Core.Interfaces;
using Core.Model;
using Core.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using NLog.Extensions.Logging;

namespace SysTechBackend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            var connectionString = Configuration.GetConnectionString("Database");
            services.AddDbContext<EmployeeContext>(x=>x.UseSqlite(connectionString));
            services.Add(ServiceDescriptor.Scoped<IEmployeeRepository, EmployeeRepository>());
            services.Add(ServiceDescriptor.Scoped<IEmployeeModelRepository, EmployeeModelRepository>());
            services.Add(ServiceDescriptor.Scoped<ICompanyModel, CompanyModel>());
            AutomapperInit.Init();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, EmployeeContext context)
        {
            loggerFactory.AddConsole();
            loggerFactory.AddNLog();

            if (env.IsDevelopment())
            {
                context.SeedDatabase();
                loggerFactory.AddDebug(LogLevel.Debug);
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
