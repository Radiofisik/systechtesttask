﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.DTO;
using Core.Interfaces;
using Core.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SysTechBackend.Controllers
{
    [Produces("application/json")]
    [Route("api/employee")]
    public class EmployeeController : Controller
    {
        private readonly IEmployeeModelRepository employeeModelRepository;

        public EmployeeController(IEmployeeModelRepository employeeModelRepository)
        {
            this.employeeModelRepository = employeeModelRepository;
        }

        //TODO: Add pagination (start and limit params)
        // GET api/employees
        [Route("/api/employees")]
        [HttpGet]
        public IEnumerable<EmployeeDto> GetAll()
        {
            var models = employeeModelRepository.GetAll();
            return AutoMapper.Mapper.Map<IList<BaseEmployeeModel>, IList<EmployeeDto>>(models);
        }

        //TODO: Add pagination (start and limit params)
        // GET api/employees/at?date=
        [Route("/api/employees/at")]
        [HttpGet]
        public IEnumerable<EmployeeDto> GetAllAt([FromQuery] DateTime date)
        {
            var models = employeeModelRepository.GetEmployeeListAt(date);
            return AutoMapper.Mapper.Map<IList<BaseEmployeeModel>, IList<EmployeeDto>>(models);
        }

        // GET api/employee/id
        [HttpGet("{id:int}")]
        public EmployeeDto Get(int id)
        {
            var model = employeeModelRepository.Get(id);
            return AutoMapper.Mapper.Map<BaseEmployeeModel, EmployeeDto>(model);
        }

        //TODO: add error processing and logging here
        // POST api/employee
        [HttpPost]
        public EmployeeDto Post([FromBody] EmployeeDto dto)
        {
            var model = AutoMapper.Mapper.Map<EmployeeDto, BaseEmployeeModel>(dto);
            employeeModelRepository.Add(model);
            return AutoMapper.Mapper.Map<BaseEmployeeModel, EmployeeDto>(model);
        }

        //TODO: add error processing and logging here
        // Put api/employee
        [HttpPut]
        public EmployeeDto Put([FromBody] EmployeeDto dto)
        {
            var model = AutoMapper.Mapper.Map<EmployeeDto, BaseEmployeeModel>(dto);
            employeeModelRepository.Update(model);
            return AutoMapper.Mapper.Map<BaseEmployeeModel, EmployeeDto>(model);
        }

        //TODO: add error processing and logging here
        // Delete api/employee
        [HttpDelete]
        public ActionResult Delete([FromBody] EmployeeDto dto)
        {
            var model = AutoMapper.Mapper.Map<EmployeeDto, BaseEmployeeModel>(dto);
            employeeModelRepository.Delete(model);
            return Ok();
        }
    }
}