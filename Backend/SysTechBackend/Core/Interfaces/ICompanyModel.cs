﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Interfaces
{
    public interface ICompanyModel
    {
        decimal GetCommonSalary(DateTime date);
    }
}
