﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Entity;

namespace Core.Interfaces
{
    public interface IEmployeeRepository: IRepository<BaseEmployee>
    {
    }
}
