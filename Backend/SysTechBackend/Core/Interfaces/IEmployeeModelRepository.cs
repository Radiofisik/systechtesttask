﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Model;

namespace Core.Interfaces
{
    public interface IEmployeeModelRepository: IRepository<BaseEmployeeModel>
    {
        IList<BaseEmployeeModel> GetEmployeeListAt(DateTime date);
    }
}
