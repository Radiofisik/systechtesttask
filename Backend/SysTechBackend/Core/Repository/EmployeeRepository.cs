﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.DAL;
using Core.Entity;
using Core.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Core.Repository
{
    public class EmployeeRepository: BaseRepository<BaseEmployee>, IEmployeeRepository
    {
        public EmployeeRepository(EmployeeContext context) : base(context)
        {
        }
    }
}
