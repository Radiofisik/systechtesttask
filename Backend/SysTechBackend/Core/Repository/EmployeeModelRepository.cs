﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using Core.Entity;
using Core.Interfaces;
using Core.Model;

namespace Core.Repository
{
    public class EmployeeModelRepository: IEmployeeModelRepository
    {
        private readonly IEmployeeRepository entityRepository;

        public EmployeeModelRepository(IEmployeeRepository entityRepository)
        {
            this.entityRepository = entityRepository;
        }

        public IList<BaseEmployeeModel> GetAll()
        {
            var entities = entityRepository.GetAll();
            var models = AutoMapper.Mapper.Map<IList<BaseEmployee>, IList<BaseEmployeeModel>>(entities);
            return models;
        }

        public void Add(BaseEmployeeModel entity)
        {
            var actualEntity=AutoMapper.Mapper.Map<BaseEmployeeModel, BaseEmployee>(entity);
            entityRepository.Add(actualEntity);
        }

        public void Update(BaseEmployeeModel entity)
        {
            var actualEntity = AutoMapper.Mapper.Map<BaseEmployeeModel, BaseEmployee>(entity);
            entityRepository.Update(actualEntity);
        }

        public void Delete(BaseEmployeeModel entity)
        {
            var actualEntity = AutoMapper.Mapper.Map<BaseEmployeeModel, BaseEmployee>(entity);
            entityRepository.Delete(actualEntity);
        }

        public IList<BaseEmployeeModel> GetEmployeeListAt(DateTime date)
        {
            var entities = entityRepository.GetAll();
            var models = AutoMapper.Mapper.Map<IList<BaseEmployee>, IList<BaseEmployeeModel>>(entities).Select(x =>
            {
                x.CurrentDate = date;
                return x;
            }).Where(x=>x.HireDate<date).ToList(); //can be moved to database layer to improve performance
            return models;
        }

        public BaseEmployeeModel Get(int id)
        {
            var entitity = entityRepository.Get(id);
            var model = AutoMapper.Mapper.Map<BaseEmployee, BaseEmployeeModel>(entitity);
            return model;
        }
    }
}
