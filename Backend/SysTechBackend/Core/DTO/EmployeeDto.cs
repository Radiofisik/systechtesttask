﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Core.DTO
{
//    TODO: add validation
    public class EmployeeDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("managerId")]
        public int? ManagerId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("hireDate")]
        public DateTime HireDate { get; set; }

        [JsonProperty("baseSalary")]
        public Decimal BaseSalary { get; set; }

        [JsonProperty("currentSalary")]
        public Decimal CurrentSalary { get; set; }

        [JsonProperty("hasSubEmployees")]
        public bool HasSubemployees { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public EmployeeType Type { get; set; }
    }
}
