﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.DTO
{
    public enum EmployeeType
    {
        Employee,
        Manager,
        Salesman
    }
}
