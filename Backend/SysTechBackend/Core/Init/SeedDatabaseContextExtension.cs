﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL;
using Core.Entity;

namespace Core.Init
{
    public static class SeedDatabaseContextExtension
    {
        public static void SeedDatabase(this EmployeeContext context)
        {
            if(context.BaseEmployees.Any()) return;

            var director = new Manager()
            {
                BaseSalary = 500000,
                Name = "Director",
                HireDate = DateTime.Today - TimeSpan.FromDays(366 * 4)
            };
            context.BaseEmployees.Add(director);

            var manager = new Manager()
            {
                BaseSalary = 300000,
                Name = "Main manager",
                HireDate = DateTime.Today - TimeSpan.FromDays(366 * 3),
                Manager = director
            };
            context.BaseEmployees.Add(manager);

                var emp1 = new Employee()
                {
                    BaseSalary = 100000,
                    Name = "Employee of first manager 1",
                    HireDate = DateTime.Today - TimeSpan.FromDays(366 * 3),
                    Manager = manager
                };
                context.BaseEmployees.Add(emp1);

                var emp2 = new Employee()
                {
                    BaseSalary = 100000,
                    Name = "Employee of first manager 2",
                    HireDate = DateTime.Today - TimeSpan.FromDays(366 * 3),
                    Manager = manager
                };
                context.BaseEmployees.Add(emp2);

                var emp3 = new Employee()
                {
                    BaseSalary = 100000,
                    Name = "Employee of first manager 3",
                    HireDate = DateTime.Today - TimeSpan.FromDays(366 * 3),
                    Manager = manager
                };
                context.BaseEmployees.Add(emp3);

            var secretary = new Employee()
            {
                BaseSalary = 200000,
                Name = "Secretary",
                HireDate = DateTime.Today - TimeSpan.FromDays(366 * 4),
                Manager = director
            };
            context.BaseEmployees.Add(secretary);

            var salesman = new Salesman()
            {
                BaseSalary = 300000,
                Name = "Main Salesman",
                HireDate = DateTime.Today - TimeSpan.FromDays(366 * 4),
                Manager = director
            };
            context.BaseEmployees.Add(salesman);
                var salesman2 = new Salesman()
                {
                    BaseSalary = 100000,
                    Name = "Second Salesman",
                    HireDate = DateTime.Today - TimeSpan.FromDays(366 * 4),
                    Manager = salesman
                };
                context.BaseEmployees.Add(salesman2);
                    var salesman3 = new Salesman()
                    {
                        BaseSalary = 50000,
                        Name = "Junior Salesman1",
                        HireDate = DateTime.Today - TimeSpan.FromDays(366 * 4),
                        Manager = salesman
                    };
                    context.BaseEmployees.Add(salesman3);

                    var salesman4 = new Salesman()
                    {
                        BaseSalary = 50000,
                        Name = "Junior Salesman2",
                        HireDate = DateTime.Today - TimeSpan.FromDays(366 * 4),
                        Manager = salesman
                    };
                    context.BaseEmployees.Add(salesman4);

            context.SaveChanges();

        }
    }
}
