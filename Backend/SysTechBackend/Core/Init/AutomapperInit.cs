﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using AutoMapper;
using Core.DTO;
using Core.Entity;
using Core.Model;

namespace Core.Init
{
    public class AutomapperInit
    {
        public static void Init()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<BaseEmployee, BaseEmployeeModel>()
                    .Include<Employee, EmployeeModel>()
                    .Include<Salesman, SalesmanModel>()
                    .Include<Manager, ManagerModel>().ReverseMap();
                cfg.CreateMap<Employee, EmployeeModel>().ReverseMap();
                cfg.CreateMap<Salesman, SalesmanModel>().ReverseMap();
                cfg.CreateMap<Manager, ManagerModel>().ReverseMap();

                cfg.CreateMap<BaseEmployeeModel, EmployeeDto>()
                    .Include<EmployeeModel, EmployeeDto>()
                    .Include<ManagerModel, EmployeeDto>()
                    .Include<SalesmanModel, EmployeeDto>();
                cfg.CreateMap<EmployeeModel, EmployeeDto>().AfterMap((x, y) => y.Type = EmployeeType.Employee);
                cfg.CreateMap<ManagerModel, EmployeeDto>().AfterMap((x, y) => y.Type = EmployeeType.Manager);
                cfg.CreateMap<SalesmanModel, EmployeeDto>().AfterMap((x, y) => y.Type = EmployeeType.Salesman);

                //dto reverse mapping
                cfg.CreateMap<EmployeeDto, EmployeeModel>();
                cfg.CreateMap<EmployeeDto, ManagerModel>();
                cfg.CreateMap<EmployeeDto, SalesmanModel>();
                cfg.CreateMap<EmployeeDto, BaseEmployeeModel>().ConvertUsing((dto) =>
                {
                    switch (dto.Type)
                    {
                        case EmployeeType.Employee:
                            return Mapper.Map<EmployeeDto, EmployeeModel>(dto);
                        case EmployeeType.Manager:
                            return Mapper.Map<EmployeeDto, ManagerModel>(dto);
                        case EmployeeType.Salesman:
                            return Mapper.Map<EmployeeDto, SalesmanModel>(dto);
                        default:
                            return null;
                    }
                });
            });

        }

        public static void Reset()
        {
            Mapper.Reset();
        }
    }
}