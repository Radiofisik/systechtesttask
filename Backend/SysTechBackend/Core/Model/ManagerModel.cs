﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Model
{
    public class ManagerModel : BaseEmployeeModel
    {
        //TODO: make them configurable
        public const decimal MaximumExperiencePercent = 40;
        public const decimal AnnualExperiencePercent = 5;
        public const decimal ManagerPercent = 0.5M;

        protected override decimal GetExperienceBonusWithoutMaximum(int experienceInYears)
        {
            return experienceInYears * BaseSalary * AnnualExperiencePercent / 100;
        }

        protected override decimal GetManagerBonus(DateTime date)
        {
            return SumSubEmployeeSalaryAt(date) * ManagerPercent / 100;
        }

        protected override decimal GetMaximumExperienceBonus()
        {
            return BaseSalary * MaximumExperiencePercent / 100;
        }
    }
}
