﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Model
{
    public class SalesmanModel : BaseEmployeeModel
    {
        //TODO: make them configurable
        public const decimal MaximumExperiencePercent = 35;
        public const decimal AnnualExperiencePercent = 1;
        public const decimal ManagerPercent = 0.3M;

        protected override decimal GetExperienceBonusWithoutMaximum(int experienceInYears)
        {
            return experienceInYears * BaseSalary * AnnualExperiencePercent / 100;
        }

        protected override decimal GetManagerBonus(DateTime date)
        {
            return SumSubEmployeeSalaryRecursiveAt(date) * ManagerPercent / 100;
        }

        protected override decimal GetMaximumExperienceBonus()
        {
            return BaseSalary * MaximumExperiencePercent / 100;
        }
    }
}
