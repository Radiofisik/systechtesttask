﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Model
{

    public class EmployeeModel : BaseEmployeeModel
    {
        //TODO: make them configurable
        public const decimal MaximumExperiencePercent = 30;
        public const decimal AnnualExperiencePercent = 3;

        protected override decimal GetExperienceBonusWithoutMaximum(int experienceInYears)
        {
            return experienceInYears * BaseSalary * AnnualExperiencePercent / 100 ;
        }

        protected override decimal GetManagerBonus(DateTime date)
        {
            return 0;
        }

        protected override decimal GetMaximumExperienceBonus()
        {
            return BaseSalary * MaximumExperiencePercent / 100;
        }
    }
}
