﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Interfaces;

namespace Core.Model
{
    public class CompanyModel: ICompanyModel
    {
        //I dont use _something notation for private fields because it is deprecated according to book Cleancode of Robert C. Martin
        private readonly IEmployeeModelRepository employeeModelRepository;
        public CompanyModel(IEmployeeModelRepository employeeModelRepository)
        {
            this.employeeModelRepository = employeeModelRepository;
        }

        public decimal GetCommonSalary(DateTime date)
        {
            return employeeModelRepository.GetAll().Sum(x => x.GetSalaryAt(date));
        }
    }
}
