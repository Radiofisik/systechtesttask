﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Model
{
    public abstract class BaseEmployeeModel
    {
        public int Id { get; set; }

        public int? ManagerId { get; set; }

        public string Name { get; set; }

        public DateTime HireDate { get; set; }

        public Decimal BaseSalary { get; set; } 

        public DateTime CurrentDate { get; set; } = DateTime.Now;

        public Decimal CurrentSalary => GetSalaryAt(CurrentDate);

        public bool HasSubemployees => SubEmployees.Count > 0;

        public ICollection<BaseEmployeeModel> SubEmployees { get; set; } = new List<BaseEmployeeModel>();

        public BaseEmployeeModel Manager { get; set; }

        protected abstract Decimal GetExperienceBonusWithoutMaximum(int experienceInYears);

        protected abstract Decimal GetMaximumExperienceBonus();

        protected abstract Decimal GetManagerBonus(DateTime date);

        public virtual Decimal GetExperienceBonus(int experienceInYears)
        {
            return Math.Min(GetExperienceBonusWithoutMaximum(experienceInYears), GetMaximumExperienceBonus());
        }

        public int GetExperienceInYears(DateTime date)
        {
            //TODO: consult lawer about this
            return (int)((date-HireDate).TotalDays / 365.2425);
        }

        public virtual Decimal GetSalaryAt(DateTime date)
        {
            if (HireDate > date) return 0;
            return BaseSalary + GetExperienceBonus(GetExperienceInYears(date)) + GetManagerBonus(date);
        }

        public decimal SumSubEmployeeSalaryAt(DateTime date)
        {
            return SubEmployees.Sum(x => x.GetSalaryAt(date));
        }

        public decimal SumSubEmployeeSalaryRecursiveAt(DateTime date)
        {
            return SubEmployees.Sum(x => x.GetSalaryAt(date) + x.SumSubEmployeeSalaryRecursiveAt(date));
        }
    }
}
