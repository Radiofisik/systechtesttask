﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Core.Entity
{
    public class BaseEmployee
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        public DateTime HireDate { get; set; }

        public Decimal BaseSalary { get; set; }

        public ICollection<BaseEmployee> SubEmployees { get; set; } = new List<BaseEmployee>();

        [ForeignKey("ManagerId")]
        public BaseEmployee Manager { get; set; }

        public int? ManagerId { get; set; }
    }
}
