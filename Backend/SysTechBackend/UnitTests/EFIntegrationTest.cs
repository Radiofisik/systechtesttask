﻿using Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Core.DAL;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Xunit;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Xunit.Priority;

namespace UnitTests
{

    public class EFIntegrationTest
    {
        private DbContextOptions<EmployeeContext> dbContextOptions;

        public EFIntegrationTest()
        {
            var connectionStringBuilder = new SqliteConnectionStringBuilder
             { DataSource = ":memory:" };
            //    { DataSource = "testbd" };
            var connectionString = connectionStringBuilder.ToString();

            var connection = new SqliteConnection(connectionString);
            connection.Open();
            var builder = new DbContextOptionsBuilder<EmployeeContext>();
            builder.UseSqlite(connection);
            dbContextOptions = builder.Options;
        }

        private List<BaseEmployee> GenerateTestData()
        {
            var list = new List<BaseEmployee>();
            var manager = new Manager() { Name = "Manager 1", BaseSalary = 200, HireDate = DateTime.Today };
            list.Add(manager);
            for (int i = 1; i < 10; i++)
            {
                list.Add(new Employee(){Name = $"Employee number {i} of Manager 1", BaseSalary = i*10, HireDate = DateTime.Today});
            }

            for (int i = 1; i < 3; i++)
            {
                list.Add(new Salesman() { Name = $"SalesMan number {i} of Manager 1", BaseSalary = i * 10, HireDate = DateTime.Today });
            }
            return list;
        }

        [Fact]
        public void CreateEmployee()
        {
            using (var context = new EmployeeContext(dbContextOptions))
            {

                var employee = new Employee() {Name = "Test", BaseSalary = 100, HireDate = DateTime.Today};
                context.Employees.Add(employee);
                context.SaveChanges();
            }
        }

        [Fact]
        public void CreateManager()
        {
            using (var context = new EmployeeContext(dbContextOptions))
            {

                var employee = new Manager() { Name = "Test", BaseSalary = 100, HireDate = DateTime.Today };
                context.Managers.Add(employee);
                context.SaveChanges();
            }
        }

        [Fact]
        public void CreateSalesMan()
        {
            using (var context = new EmployeeContext(dbContextOptions))
            {

                var employee = new Salesman() { Name = "Test", BaseSalary = 100, HireDate = DateTime.Today };
                context.SalesMen.Add(employee);
                context.SaveChanges();
            }
        }

        [Fact]
        public void CheckBaseEmployees()
        {
            using (var context = new EmployeeContext(dbContextOptions))
            {

                var data = GenerateTestData();
                context.BaseEmployees.AddRange(data);
                context.SaveChanges();
            }

            using (var context = new EmployeeContext(dbContextOptions))
            {
                var people = context.BaseEmployees.ToList();
                Assert.NotEmpty(people);
            }
        }
    }
}
