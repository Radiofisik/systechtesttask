﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Model;
using Xunit;

namespace UnitTests
{
    public class BaseEmployeeModelTest
    {
        public BaseEmployeeModelTest()
        {
        }

        [Fact]
        public void GetSalaryAtTestWithoutLimit()
        {
            var employee = new EmployeeModel() {BaseSalary = 100, HireDate = DateTime.Now - TimeSpan.FromDays(366 * 2)};
            Assert.Equal(employee.BaseSalary + employee.BaseSalary * 2 * EmployeeModel.AnnualExperiencePercent / 100,
                employee.GetSalaryAt(DateTime.Now));
        }

        [Fact]
        public void GetSalaryAtTestMaximumLimit()
        {
            var employee = new EmployeeModel() { BaseSalary = 100, HireDate = DateTime.Now - TimeSpan.FromDays(366 * 1000) };
            Assert.Equal(employee.BaseSalary + employee.BaseSalary * EmployeeModel.MaximumExperiencePercent / 100,
                employee.GetSalaryAt(DateTime.Now));
        }

        [Fact]
        public void SumSubEmployeeSalaryAtTest()
        {
            var manager =new ManagerModel();
            var employeeSalary =
                new EmployeeModel() {BaseSalary = 100, HireDate = DateTime.Now - TimeSpan.FromDays(366 * 1000)}
                    .GetSalaryAt(DateTime.Now);
            manager.SubEmployees.Add(new EmployeeModel() { BaseSalary = 100, HireDate = DateTime.Now - TimeSpan.FromDays(366 * 1000) });
            manager.SubEmployees.Add(new EmployeeModel() { BaseSalary = 100, HireDate = DateTime.Now - TimeSpan.FromDays(366 * 1000) });
            manager.SubEmployees.Add(new EmployeeModel() { BaseSalary = 100, HireDate = DateTime.Now - TimeSpan.FromDays(366 * 1000) });
            manager.SubEmployees.Add(new EmployeeModel() { BaseSalary = 100, HireDate = DateTime.Now - TimeSpan.FromDays(366 * 1000) });
            manager.SubEmployees.Add(new EmployeeModel() { BaseSalary = 100, HireDate = DateTime.Now - TimeSpan.FromDays(366 * 1000) });
            Assert.Equal(employeeSalary*5, manager.SumSubEmployeeSalaryAt(DateTime.Now));
        }

        [Fact]
        public void SumSubEmployeeRecursiveSalaryAtTest()
        {
            var manager = new ManagerModel();
            var employeeSalary =
                new EmployeeModel() { BaseSalary = 100, HireDate = DateTime.Now - TimeSpan.FromDays(366 * 2) }
                    .GetSalaryAt(DateTime.Now);
            manager.SubEmployees.Add(new EmployeeModel() { BaseSalary = 100, HireDate = DateTime.Now - TimeSpan.FromDays(366 * 2) });
            manager.SubEmployees.Add(new EmployeeModel() { BaseSalary = 100, HireDate = DateTime.Now - TimeSpan.FromDays(366 * 2) });
            manager.SubEmployees.Add(new EmployeeModel() { BaseSalary = 100, HireDate = DateTime.Now - TimeSpan.FromDays(366 * 2) });
            manager.SubEmployees.Add(new EmployeeModel() { BaseSalary = 100, HireDate = DateTime.Now - TimeSpan.FromDays(366 * 2) });
            manager.SubEmployees.Add(new EmployeeModel() { BaseSalary = 100, HireDate = DateTime.Now - TimeSpan.FromDays(366 * 2) });

            var salesman=new SalesmanModel() { BaseSalary = 100, HireDate = DateTime.Now - TimeSpan.FromDays(366 * 2) };
            salesman.SubEmployees.Add(manager);
            Assert.Equal(employeeSalary * 5+manager.GetSalaryAt(DateTime.Now), salesman.SumSubEmployeeSalaryRecursiveAt(DateTime.Now));
        }
    }
}