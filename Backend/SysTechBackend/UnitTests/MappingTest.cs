using System;
using Core.DTO;
using Core.Entity;
using Core.Init;
using Core.Model;
using Xunit;

namespace UnitTests
{
    public class MappingTest: IDisposable
    {
        public MappingTest()
        {
            AutomapperInit.Init();
        }

        public void Dispose()
        {
            AutomapperInit.Reset();
        }

        [Fact]
        public void PolymorphicMappingTest()
        {
            var entity = new Manager();
            var model = AutoMapper.Mapper.Map<BaseEmployee, BaseEmployeeModel>(entity);
            Assert.IsType<ManagerModel>(model);
        }

        [Fact]
        public void PolymorphicReverseMappingTest()
        {
            var entity = new Manager();
            var model = AutoMapper.Mapper.Map<BaseEmployee, BaseEmployeeModel>(entity);
            var mappedEntity = AutoMapper.Mapper.Map<BaseEmployeeModel, BaseEmployee>(model);
            Assert.IsType<Manager>(mappedEntity);
        }

        [Fact]
        public void DtoMappingTest()
        {
            var model=new ManagerModel();
            var dto = AutoMapper.Mapper.Map<BaseEmployeeModel, EmployeeDto>(model);
            Assert.Equal(EmployeeType.Manager, dto.Type);
        }

        [Fact]
        public void DtoReverseMappingTest()
        {
            var dto = new EmployeeDto() {Type = EmployeeType.Manager};
            var model = AutoMapper.Mapper.Map<EmployeeDto, BaseEmployeeModel>(dto);
            Assert.IsType<ManagerModel>(model);
        }

    }
}
